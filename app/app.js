var app = angular.module('homework', []);

app.controller('MainCtrl', function(UsersLoad, LangLoad, RoleLoad) {
  var _this = this;

  _this.loadSteps = {
      start: 0,
      step: 10,
      end: 10
  }

  _this.users = [];
  _this.current = {};
  _this.selected = [];

  _this.languages = LangLoad;
  _this.roles = RoleLoad;

  _this.filters = {};

  _this.sortType     = 'name';
  _this.sortReverse  = false;

  _this.loading = false;

  _this.loadMore = () => {
   _this.loading = true;
   UsersLoad.getUsers(_this.loadSteps.start, _this.loadSteps.end).then(function(response) {
        _this.loading = false;
        _this.users = _this.users.concat(response.data.data);
        _this.loadSteps.start+=10;
        _this.loadSteps.step+=10;
        _this.loadSteps.end+=10;
        _this.loadBtnEnable = (140 !== _this.loadSteps.end);
   })

 }

 _this.deleteItem = (index) => {
   for(var i=0; i<_this.users.length; i++) {
     if(_this.users[i]['id'] == index) {
         _this.users.splice(i, 1);
     }
   }
 }


 _this.select = (id) => {
    var found = _this.selected.indexOf(id);
    if(found == -1) {
       _this.selected.push(id);
    } else {
      _this.selected.splice(found, 1);
    }
 }

 _this.deleteUsers = () => {
    for(var i=0; i<=_this.selected.length; i++) {
      _this.deleteItem(_this.selected[i]);
    }
 }

 _this.changeFiltering = () => {
   console.log("::: changeFiltering");
 }

 _this.useFiltering = () => {
   console.log("::::::: _this.filters", _this.filters);
   console.log(":::: applyFiltering");
 }

 _this.resetFiltering = () => {
   _this.users = [];
   _this.loadMore();
 }

 _this.loadMore();

});


app.service('UsersLoad', function($http) {
  this.getUsers = (start, end) => {
    return $http.get('/api/users', {params:{start:start, end:end, order: "desc"}});
  }

});

app.service('LangLoad', function() {
  return {
    'EN':'English',
    'DE':'German',
    'IT':'Italian',
    'FR':'French'
 }
});

app.service('RoleLoad', function() {
  return {
    'VI':'Victory',
    'AU':'Author',
    'SU':'Superadmin',
    'AD':'Administrator',
    'UP':'Upperintermediate'
 }
});

app.filter('formatingLang', function(LangLoad) {
    return function(text){
      return LangLoad[text];
    }
});

app.filter('formatingRole', function(RoleLoad) {
    return function(text){
      return RoleLoad[text];
    }
});

app.filter('formatingDate', function() {
  return function(text) {
    return moment(text).startOf('minute').fromNow();
  }
})

app.directive('users', function() {
  return {
    restrict: 'E',
    replace: true,
    scope: {},
    templateUrl: 'vk_table_template.html',
    controller: "MainCtrl",
    controllerAs: "main"
  }
});

app.directive('loader', function() {
   return {
     restrict: 'E',
     replace: true,
     templateUrl: 'vk_loader.html'
   }
})

app.directive('filter', function() {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: 'vk_filter.html'
  }
})
