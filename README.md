# Home Work

This is a homework task for weekly AngularJS workshop. Due date for this task is Thursday (I need some time to check all your results and make some cocnlusions, remarks, etc), so please get things ready before that day.



## How to start

1. Create an account on BitBucket.
2. Fork this repo.
3. Install Git client. I'd recomment [Git console](https://git-scm.com/download/win) but it's up to you.
4. Clone forked repo to your computer.
5. Install [NodeJS](https://nodejs.org/en).
6. Navigate to `ngHomeWork` folder.
7. Run `npm install` to install the dependancies, listed in `packaje.json`. Wait for installation to be finished.
8. Run `npm start` to start the server.
9. Open `http://localhost:8080/` in your browser.
10. Implement the app.
11. Commit your changes. Push it to the repo. Send me a pull request.



## API


### 1. Get users list

GET: `/api/users?<query params>`

| Param                 | Default          | Description                                     | Example                                  |
| --------------------- | ---------------- | ----------------------------------------------- | ---------------------------------------- |
| **start**: Number     | 0                | Page absolute start offset                      | /api/users?start=5                       |
| **end**: Number       | 5                | Page absolute end offset                        | /api/users?end=20                        |
| **sort**: String      | 'updated'        | Sorting field name                              | /api/users?sort=created                  |
| **order**: Number     | 'desc'           | Sorting order ('desc' or 'asc')                 | /api/users?order=asc                     |
| **<filters>**         |  None            | Set of filtering options                        | /api/users?name=Denis&surname=Mamrak     |

Returns users list along with some metadata in JSON format.


### 2. Get user

GET: `/api/users/<id>`

| Param             | Default          | Description                                     | Example                             |
| ----------------- | ---------------- | ----------------------------------------------- | ----------------------------------- |
| **id**: Number    | None             | Unique user ID                                  | /api/users/100                      |

Returns user data in JSON format.


### 3. Create user

 POST: `/api/users/`

| Param                    | Default          | Description                                     | Example                                        |
| ------------------------ | ---------------- | ----------------------------------------------- | ---------------------------------------------- |
| **<user data>**: JSON    | None             | JSON object with user data to be saved          | {name: 'Denis', surname: 'Mamrak'}             |

Returns data for newly created user with id and timestamps in JSON format.


### 4. Update user

 PATCH: `/api/users/<id>`

| Param                    | Default          | Description                                     | Example                                        |
| ------------------------ | ---------------- | ----------------------------------------------- | ---------------------------------------------- |
| **id**: Number           | None             | Unique user ID                                  | /api/users/100                                 |
| **<user data>**: JSON    | None             | JSON object with user data to be updated        | {name: 'Victor', surname: 'Tsoi'}              |

Returns data for updated user with new 'updated' timestamp in JSON format.


### 5. Delete user

DELETE: `/api/users/<id>`

| Param             | Default          | Description                                     | Example                             |
| ----------------- | ---------------- | ----------------------------------------------- | ----------------------------------- |
| **ids**: List     | None             | Semicolon separated user ID list                | /api/users/10;20;30                 |

Doesn't return any response.



## Homework task (8 Apr 2016)

The application should meet the following criteria:

- Load first 10 users on startup and render them as a table.
- Render users list in order of appearance in the server response.
- Change the sort order accordingly when user clicks on column header. Default order should be ascending. Further clicks should toggle the order.
- `Language` and `Role` values come as value codes, not as human readable values. Beautify them, preferably without changing the original model.
- `Created` and `Updated` values come as a timestamp, make them human readable, live the original data intact.
- Load next 10 users after click on `Load more` and append them to previuosly loaded.
- Hide `Load more` button when the end of the list is reached.


## Some hints

### General instructions

Keep things simple and concentrate on functionality, not architecture: this task doesn't require sophisticated solutions. Ideally app should look like [this](https://i.gyazo.com/b803833ad126dfce46a20d83248b3029.png), but, again, don't concentrate on HTML and CSS, main goal is to implement JS part.

### Sort order icon

There's no need to use raster image icon to indicate current corting order, you can use unicode symbol along with `:after` pseudo element:

```
a.asc:after {
	content: "▲";
}

a.desc:after {
	content: "▼";
}

```

### Beautifying timestamps

This may be somewhat complicated, to make this task easier I'd recommend you a great tool [Moment.js](http://momentjs.com/).



## Homework task (15 Apr 2016)

First of all you need to updated yoor forked repo in order to get recent changes, made to this description and the backend part. [Here](http://stackoverflow.com/questions/9944898/bitbucket-update-a-fork-to-merge-changes-of-master-repo) are some ideas how to do this.

In addition to previously implemented funtionality the application should meet the following criteria:

- Show loading indicator, blocking the UI when app is performing the request. ngDemo contains a good example, take a look at it.
- Let's use server-side ordering instead of client-side. See the API description for details. Default sorting field should be 'updated' with 'desc' order. Reordering should completely refresh the table and load first 10 users.
- Let's use server-side filtering. See the API description for details. Create Filters control, filtering users list on `Name`, `Surname`, `Email`, `Role` and `Language` fields. `Filter` button should apply filters and `Reset` should reset filters. Applying / ressetting filters should completely refresh the table and load first 10 users.
- Each table row should have it's own `Delete` button. After successful deletion the relevant user should be removed from the table without complete table refreshment.
- Each table row should have it's own checkbox to select / deselect multiple rows. Selected rows should be highlighted in any way you like (background color, bolder font etc.)
- There should be a button `Delete users` above table. Click on this button should delete all selected users and, after successfull deletion, remove them from the table without complete refreshment. If no users selected button should be disabled.
- `Created` and `Updated` timestamps should be humanized to look like `45 minutes ago` or `2 days ago` instead of exact time and date. Take [moment.js](http://momentjs.com/) into consideration. [Here](https://i.gyazo.com/7624eedc2d2f5d924a13aeb920fc25fa.png) is the basic idea of how this can look.
